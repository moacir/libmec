#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "json.h"
#include "mec_types.h"
#include "mec_parser.h"
#include "mec.h"

int main(int argc, char **argv) {
    int i=0;
    char *json_str = NULL;

    json_value *json = NULL;
    json_value *json_movies = NULL;

    movie_list_t *list;
    size_t list_length;

    json_str = mec_load_data();

    if (json_str == NULL) {
        fprintf(stderr, "Not able to download JSON\n");
        return 1;
    }

    json = json_parse(json_str, strlen(json_str));

    printf("JSON Type: %d\n", json->type);
    printf("JSON Value: %s\n", json->u.object.values[0].value->u.array.values[0]->u.object.values[0].name);

    json_movies = json->u.object.values[0].value;

/*
    for (i=0; i<json_movies->u.array.length; i++) {
        for (j=0; j<json_movies->u.array.values[i]->u.object.length; j++) {
            printf("name: %s => %s\n",
                json_movies->u.array.values[i]->u.object.values[j].name,
                json_movies->u.array.values[i]->u.object.values[j].value->u.string.ptr
            );
        }

        printf("========================================\n");
    }
*/
    list = mec_parse_movies(json_movies);
    list_length = mec_movie_list_length(list);

    for (i=0; i<list_length; i++) {
        printf("Movie: %s | %d\n", mec_movie_get_title(mec_movie_list_get(list, i)), mec_movie_get_id_movie(mec_movie_list_get(list, i)));
    }

    free(json_str);
    json_value_free(json);

    mec_movie_list_free(list);

    return 0;
}
