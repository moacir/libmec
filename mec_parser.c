#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mec_parser.h"

static movie_t * parse_movie(const json_value *json_movie) {
    movie_t *movie;
    json_object_entry json_attr;
    char *attr;
    int i;
    size_t length;

    movie = mec_movie_create();

    if (!movie || !json_movie) return NULL;

    length = json_movie->u.object.length;

    for (i=0; i<length; i++) {
        json_attr = json_movie->u.object.values[i];
        attr = json_attr.name;

        if (strcmp(attr, "idMovie") == 0) mec_movie_set_id_movie(movie, atoi(json_attr.value->u.string.ptr));
        if (strcmp(attr, "slug") == 0) mec_movie_set_slug(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "title") == 0) mec_movie_set_title(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "originalTitle") == 0) mec_movie_set_original_title(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "synopsis") == 0) mec_movie_set_synopsis(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "trailer") == 0) mec_movie_set_trailer(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "image") == 0) mec_movie_set_image(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "featured") == 0) mec_movie_set_featured(movie, atoi(json_attr.value->u.string.ptr));
        if (strcmp(attr, "duration") == 0) mec_movie_set_duration(movie, atoi(json_attr.value->u.string.ptr));
        if (strcmp(attr, "releaseDate") == 0) mec_movie_set_release_date(movie, atoi(json_attr.value->u.string.ptr));
        if (strcmp(attr, "censorship") == 0) mec_movie_set_censorship(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "director") == 0) mec_movie_set_director(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "actors") == 0) mec_movie_set_actors(movie, json_attr.value->u.string.ptr);
        if (strcmp(attr, "order") == 0) mec_movie_set_order(movie, atoi(json_attr.value->u.string.ptr));
    }

    return movie;
}

movie_list_t * mec_parse_movies(const json_value *json_movies) {
    movie_list_t *list;
    movie_t *movie;
    size_t size;
    int i;

    if (!json_movies) return NULL;

    size = json_movies->u.array.length;

    list = mec_movie_list_create(size);

    if (!list) return NULL;

    for (i=0; i<size; i++) {
        movie = parse_movie(json_movies->u.array.values[i]);
        mec_movie_list_set(list, i, movie);
    }

    return list;
}

theater_list_t * mec_parse_theaters(const json_value *theaters) {
    return NULL;
}
