#ifndef _MEC_TYPES_H
#define _MEC_TYPES_H

struct movie_s;
typedef struct movie_s movie_t;

struct theater_s;
typedef struct theater_s theater_t;

struct session_s;
typedef struct session_s session_t;

struct movie_list_s;
typedef struct movie_list_s movie_list_t;

struct theater_list_s;
typedef struct theater_list_s theater_list_t;

//=======================================================
// Movie
//=======================================================

movie_t * mec_movie_create();
int mec_movie_free(movie_t *);

int mec_movie_get_id_movie(const movie_t *);
char * mec_movie_get_slug(const movie_t *);
char * mec_movie_get_title(const movie_t *);
char * mec_movie_get_original_title(const movie_t *);
char * mec_movie_get_synopsis(const movie_t *);
char * mec_movie_get_trailer(const movie_t *);
char * mec_movie_get_image(const movie_t *);
int mec_movie_get_featured(const movie_t *);
int mec_movie_get_duration(const movie_t *);
int mec_movie_get_release_date(const movie_t *);
char * mec_movie_get_censorship(const movie_t *);
char * mec_movie_get_director(const movie_t *);
char * mec_movie_get_actors(const movie_t *);
int mec_movie_get_order(const movie_t *);

int mec_movie_set_id_movie(movie_t *, int);
int mec_movie_set_slug(movie_t *, const char *);
int mec_movie_set_title(movie_t *, const char *);
int mec_movie_set_original_title(movie_t *, const char *);
int mec_movie_set_synopsis(movie_t *, const char *);
int mec_movie_set_trailer(movie_t *, const char *);
int mec_movie_set_image(movie_t *, const char *);
int mec_movie_set_featured(movie_t *, int);
int mec_movie_set_duration(movie_t *, int);
int mec_movie_set_release_date(movie_t *, int);
int mec_movie_set_censorship(movie_t *, const char *);
int mec_movie_set_director(movie_t *, const char *);
int mec_movie_set_actors(movie_t *, const char *);
int mec_movie_set_order(movie_t *, int);

//=======================================================
// Movie List
//=======================================================

movie_list_t * mec_movie_list_create(size_t);
int mec_movie_list_free(movie_list_t *);

int mec_movie_list_set(movie_list_t *, size_t, movie_t *);
movie_t * mec_movie_list_get(movie_list_t *, size_t);

size_t mec_movie_list_length(const movie_list_t *);

//=======================================================
// Theater
//=======================================================

#endif
