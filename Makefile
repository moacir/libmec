CC=gcc
CFLAGS=-Wall -g `curl-config --cflags`
LIBS=-lm `curl-config --libs`

SRC=json.c mec.c mec_types.c mec_parser.c main.c
OBJ=$(SRC:.c=.o)
APP=mec

all: $(SRC) $(APP)

$(APP): $(OBJ) 
	$(CC) $(OBJ) $(LIBS) -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

test:
	$(CC) mec_types.c test_movie.c -o test

clean:
	rm -rf *.o mec
