#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include "mec.h"

#define MEC_API_URL "http://www.manausemcartaz.com/all.json"

struct http_data {
    char *data;
    size_t size;
};

static size_t request_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;

    struct http_data *mem = (struct http_data *)userp;

    mem->data = realloc(mem->data, mem->size + realsize + 1);

    if(mem == NULL) {
        fprintf(stderr, "Not enough memory (realloc returned NULL)\n");
        return 1;
    }

    memcpy(&(mem->data[mem->size]), contents, realsize);

    mem->size += realsize;
    mem->data[mem->size+1] = '\0';

    return realsize;
}

char *mec_load_data() {
    CURL *curl;
    CURLcode res;

    struct http_data contents = {
        .data = malloc(1),
        .size = 0
    };

    char *data = NULL;

    curl_global_init(CURL_GLOBAL_NOTHING);
    curl = curl_easy_init();

    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, MEC_API_URL);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, request_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &contents);

        res = curl_easy_perform(curl);

        if (res == CURLE_OK) {
            data = contents.data;
        } else {
            fprintf(stderr, "FAILED %s\n", curl_easy_strerror(res));
            free(contents.data);
        }
    }

    curl_easy_cleanup(curl);
    curl_global_cleanup();

    return data;
}
