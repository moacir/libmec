#include <stdlib.h>
#include <string.h>
#include "mec_types.h"

#define SET_MOVIE_CHAR_VALUE(attr, value) \
    do { \
        if (!movie || !(value)) return result; \
        if (movie->attr) free(movie->attr); \
        movie->attr = strdup(value); \
        if (movie->attr) result = 0; \
    } while(0)

#define SET_MOVIE_INT_VALUE(attr, value) \
    do { \
        if (!movie) return -1; \
        movie->attr = value; \
    } while(0)

//=======================================================
// Types
//=======================================================

struct movie_s {
    int idMovie;
    char *slug;
    char *title;
    char *originalTitle;
    char *synopsis;
    char *trailer;
    char *image;
    int featured;
    int duration;
    int releaseDate;
    char *censorship;
    char *director;
    char *actors;
    int order;
};

struct theater_s {
    int idTheater;
    char *slug;
    char *name;
    char *address;
    char *phone1;
    char *phone2;
    char *city;
    char *state;
    char *description;
    char *prices;
    char *images;
    char *foursquareId;
    char *sessionsDate;
    double latitude;
    double longitude;
};

struct session_s {
    int idMovie;
    int idTheater;
    char *type;
    int is3D;
    char *sessions;
    char *observations;
};

struct movie_list_s {
    size_t length;
    movie_t **items;
};

struct theater_list_s {
    size_t length;
    theater_t **items;
};

//=======================================================
// Movie
//=======================================================

movie_t * mec_movie_create() {
    movie_t *movie = (movie_t *)malloc(sizeof(movie_t));

    if (!movie) return NULL;

    movie->idMovie = 0;
    movie->slug = NULL;
    movie->title = NULL;
    movie->originalTitle = NULL;
    movie->synopsis = NULL;
    movie->trailer = NULL;
    movie->image = NULL;
    movie->featured = 0;
    movie->duration = 0;
    movie->releaseDate = 0;
    movie->censorship = NULL;
    movie->director = NULL;
    movie->actors = NULL;
    movie->order = 0;

    return movie;
}

int mec_movie_free(movie_t *movie) {
    if (!movie) return -1;

    if (movie->slug) free(movie->slug);
    if (movie->title) free(movie->title);
    if (movie->originalTitle) free(movie->originalTitle);
    if (movie->synopsis) free(movie->synopsis);
    if (movie->trailer) free(movie->trailer);
    if (movie->image) free(movie->image);
    if (movie->censorship) free(movie->censorship);
    if (movie->director) free(movie->director);
    if (movie->actors) free(movie->actors);

    free(movie);

    return 0;
}

/* get methods */

int mec_movie_get_id_movie(const movie_t *movie) {
    if (!movie) return -1;

    return movie->idMovie;
}

char * mec_movie_get_slug(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->slug;
}

char * mec_movie_get_title(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->title;
}

char * mec_movie_get_original_title(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->originalTitle;
}

char * mec_movie_get_synopsis(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->synopsis;
}

char * mec_movie_get_trailer(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->trailer;
}

char * mec_movie_get_image(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->image;
}

int mec_movie_get_featured(const movie_t *movie) {
    if (!movie) return -1;

    return movie->featured;
}

int mec_movie_get_duration(const movie_t *movie) {
    if (!movie) return -1;

    return movie->duration;
}

int mec_movie_get_release_date(const movie_t *movie) {
    if (!movie) return -1;

    return movie->releaseDate;
}

char * mec_movie_get_censorship(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->censorship;
}

char * mec_movie_get_director(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->director;
}

char * mec_movie_get_actors(const movie_t *movie) {
    if (!movie) return NULL;

    return movie->actors;
}

int mec_movie_get_order(const movie_t *movie) {
    if (!movie) return -1;

    return movie->order;
}

/* set methods */

int mec_movie_set_id_movie(movie_t *movie, int id_value) {
    SET_MOVIE_INT_VALUE(idMovie, id_value);

    return 0;
}

int mec_movie_set_slug(movie_t *movie, const char *slug_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(slug, slug_value);

    return result;
}

int mec_movie_set_title(movie_t *movie, const char *title_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(title, title_value);

    return result;
}

int mec_movie_set_original_title(movie_t *movie, const char *original_title_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(originalTitle, original_title_value);

    return result;
}

int mec_movie_set_synopsis(movie_t *movie, const char *synopsis_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(synopsis, synopsis_value);

    return result;
}

int mec_movie_set_trailer(movie_t *movie, const char *trailer_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(trailer, trailer_value);

    return result;
}

int mec_movie_set_image(movie_t *movie, const char *image_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(image, image_value);

    return result;
}

int mec_movie_set_censorship(movie_t *movie, const char *censorship_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(censorship, censorship_value);

    return result;
}

int mec_movie_set_director(movie_t *movie, const char *director_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(director, director_value);

    return result;
}

int mec_movie_set_actors(movie_t *movie, const char *actors_value) {
    int result = -1;

    SET_MOVIE_CHAR_VALUE(actors, actors_value);

    return result;
}

int mec_movie_set_featured(movie_t *movie, int featured_value) {
    SET_MOVIE_INT_VALUE(featured, featured_value);

    return 0;
}

int mec_movie_set_duration(movie_t *movie, int duration_value) {
    SET_MOVIE_INT_VALUE(duration, duration_value);

    return 0;
}

int mec_movie_set_release_date(movie_t *movie, int release_date_value) {
    SET_MOVIE_INT_VALUE(releaseDate, release_date_value);

    return 0;
}

int mec_movie_set_order(movie_t *movie, int order_value) {
    SET_MOVIE_INT_VALUE(order, order_value);

    return 0;
}

//=======================================================
// Movie List
//=======================================================

movie_list_t * mec_movie_list_create(size_t size) {
    movie_list_t *list = (movie_list_t *)malloc(sizeof(movie_list_t));
    int i;

    if (!list) return NULL;

    list->length = size;
    list->items = (movie_t **)calloc(size, sizeof(movie_t));

    if (!list->items) {
        free(list);
        return NULL;
    }

    for (i=0; i<list->length; i++) {
        list->items[i] = NULL;
    }

    return list;
}

int mec_movie_list_free(movie_list_t *list) {
    int i;

    if (!list) return -1;

    for (i=0; i<list->length; i++) {
        if (list->items[i]) {
            mec_movie_free(list->items[i]);
        }
    }

    free(list->items);
    free(list);

    return 0;
}

int mec_movie_list_set(movie_list_t *list, size_t index, movie_t *item) {
    if (!item || !list) return -1;

    if (index < 0 || index >= list->length) return -2;

    if (list->items[index]) {
        mec_movie_free(list->items[index]);
    }

    list->items[index] = item;

    if (!list->items[index]) {
        return -3;
    }

    return 0;
}

movie_t * mec_movie_list_get(movie_list_t *list, size_t index) {
    if (!list) return NULL;

    if (index < 0 || index >= list->length) return NULL;

    return list->items[index];
}

size_t mec_movie_list_length(const movie_list_t *list) {
    if (!list) return -1;

    return list->length;
}
