#ifndef _MEC_PARSER_H
#define _MEC_PARSER_H

#include "mec_types.h"
#include "json.h"

movie_list_t * mec_parse_movies(const json_value *);
theater_list_t * mec_parse_theaters(const json_value *);

#endif
